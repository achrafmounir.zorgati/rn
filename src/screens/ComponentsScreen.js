import React, { Component } from 'react'
import { Text, View ,StyleSheet} from 'react-native'

const ComponentsScreen = () =>{
   return  <View>
    <Text style={styles.textStyle}> Gretting Started with react native!</Text>
    <Text style={{fontSize:20}}>My name is Achraf</Text>
    </View>

}
    
         
const styles=StyleSheet.create({
    textStyle:{
        fontSize:45
    }
});

export default ComponentsScreen;